
NOTA: Se recomienda leer este archivo en el repositiorio -> https://bitbucket.org/DamCardenas/bienbasic_lyt2/src/master/Documentacion/bitacora.md

# Integrantes
|Integrante |Simbolo|
|-|-|
|Damian Cardenas Quiñones |DC|
|David Villanueva| DV|
|Karen Torrijos Galindo|KT|


## Registros
|Fecha|Integrante(s)|Accion|
|-|-|-|
|10/02/2019|DC,DV,KT|Reunion para Definicion de estrategia de trabajo. se repartieron las tareas y se puso un tiempo para terminarlas y se inicio la bitacora.|
|12/02/2019|DC| Inicia Busqueda de lenguaje base para el codigo fuente del compilador |
|12/02/2019|KT| Inicia Busqueda de Herramientas para diseño de documentacion|
|14/02/2019|DC| Se establecio la lista para posibles lenguajes de programacion base para el proyecto.|
|15/02/2019|KT| Se establecio lista para posibles herramientas de diseño de documentacion e interfaces para el proyecto|
|16/02/2019|DV| Se reviso la lista para posibles lenguajes de programacion base para el proyecto y se inicio la busqueda de utilidades y librerias para facilitar el trabajo de cada uno de los lenguajes establecidos en la lista.|
|25/02/2019|DC,DV,KT| Se realizo una junta para definir el stack de desarrollo.|
|25/02/2019|DC,DV,KT| Se establecio el siguiente stack: Java para lenguaje nucleo, Figma para diseño de interfaces y documentacion y las librerias de javaCC y YAC.|
|27/02/2019|DC,DV,KT| Se definieron las reglas lexicas, sintacticas y semanticas, se acordo que el compilador debia aceptar un lenguaje muy similar a c++ (si no es que el mismo).|
|28/02/2019|DV| Se establecieron las primeras expresiones regulares y se pusieron a prueba  en https://regex101.com/|
|28/02/2019| KT |  Se establecio un estilo de diseño para la documentacion.|
|10/03/2019| DV| Se terminaron las expresiones regulares y se almacenaron en un archivo .json para su posterior uso.|
|12/03/2019|DC| Se inicio el trabajo sobre los archivos de gramaticas de javaCC para el compilador.| 
|15/03/2019| KT,DV,DC | Se profundizo en la mejora y testing de los archivos de gramaticas.|
|15/03/2019| KT,DV,DC | Se encontraron algunos errores en las gramaticas correspondientes a los ciclos.|
|18/03/2019| KT,DV,DC| El  equipo se dio cuenta que los ciclos no eran un requisito, se opto por solo dejar el ciclo while.|
|19/03/2019|KT,DV,DC| Se encontraron dificultades tecnicas para realizar el trabajo como el equipo lo deseaba, es decir, el stack elegido entorpecia un poco las tareas que se debian llevar a cabo.|
|19/03/2019|KT,DV,DC| Se opto por desechar el proyecto empezado y empezar nuevamente desde 0, con un stack diferente y, basicamente todo de manera diferente|
|20/03/2019|KT,DV,DC| Debido a las actividades externas de cada integrante se opto por buscar un stack por demas simple y que todos dominaran para lograr obtener el avance del proyecto anterior en la menor cantidad de tiempo posible, Empezo lluvia de ideas y busquedas.|
|22/03/2019|KT,DV,DC| Se encontro un stack sencillo que hace uso de tecnologias basicas utiles para un desarrollo agil y librerias faciles de modificar (y gratuitas xD): Nodejs, electron y peg.js|
|29/03/2019|DV,DC| Se ajustaron los archivos de expresiones regulares para mejor utilizacion, simplificandolos y haciendolos de facil lectura y optimizandolos para un analisis sintactico-semantico mas sencillo y personalizado|
|24/04/2019|DV,KT,DC| Se realizaron unos cambios, dado a que se queria que el compilador fuese multiplataforma, en esto influia nuestra estrategia del compilador anterior; usar el compilador de c++ para ahorrar tiempo de trabajo, pero se acabo desechando la idea, dado a que se tendria que generar una interfaz diferente dependiendo el sistema operativo, arquitectura, etc... se opto por realizar un giro de lenguaje y el equipo se decidio por visual basic, dada su sencilles.|
|27/04/2019| KT,DC| Se establecio un nuevo diseño y se proyecto la idea de generar un IDE sencillo para usar de manera visual el compilador|
|29/04/2019|KT,DC,DV| Mediante una platica en broma, se decidio llamar al lenguaje de programacion "Bien Basic" dado a que el lenguaje era muy similar a basic y en realidad su realizacion esta basada en tecnologia "Bien Basica"|
|30/04/2019| KT,DV,DC| Se diseño un logotipo para el IDE y el compilador|
|2/05/2019| KT| Se Termino la etapa de diseño del front-end del IDE |
|3/05/2019| DC| Se Termino el analizador lexico  y su controlador|
|4/05/2019| DV| Se Integro el analizador lexico  y su controlador con el front-end|
|4/05/2019| DV,DC,KT| Se realizaron test sobre el IDE  y el analizador lexico|
|6/05/2019| DV| Se realizo el nuevo archivo de gramaticas y su implementacion con PEG.js|
|7/05/2019| DV,DC,KT|Se realizaron pruebas sobre el analisis sintactico|
|7/05/2019| DC,KT|Se realizaron adaptaciones sobre el front-end para inclusion de la etapa sintactica en el IDE|
|8/05/2019|DC| Integro la etapa sintactica al IDE de manera satisfactoria|
|8/05/2019|DV| Nos percatamos que PEG.js tiene la facilidad de modificar sus funciones internas de tal manera que se podia hacer el analisis sintactico y semantico modificando el archivo de configuracion del analizador sintactico|
|8/05/2019|DV,DC|Se modifico el analizador sintactico para que realizara un analisis semantico secuencial, se habilito la opcion de dar reportes de error|
|9/05/2019|KT| Se implemento dextery para traducir el codigo intermedio a codigo intermedio de 3 registros |
|9/05/2019|KT,DV,DC| Se realizaron pruebassobre el IDE |
|9/05/2019|DC| Se agrego la capacidad de realizar el analisis general en tiempo real |
|11/05/2019|DC,KT,DV| Se Discutieron maneras de optimizar el codigo|
|12/05/2019|DC| Se mejoro la manera de operacion de los modelos de renderizado en tiempo real, condicionando a que la interfaz solo se refresque cuando el codigo cambia|
|13/05/2019|DV| Se cambio el modelo de lectura de la tabla de simbolos, esto mejoro el tiempo de ejecucion.|
|14/05/2019|DV,DC,KT| Se acordaron las cualidades a mencionar en la exposicion final |
|14/05/2019|DC| Se inicio la produccion de un video simple para "Resaltar" las cualidades del compilador y su IDE.|
|20/05/2019|DC| Se atraso la produccion de un video simple para "Resaltar" las cualidades del compilador y su IDE.|
|23/05/2019|DC| inicio de tareas de remasterizacion de audio.|
|24/05/2019|DC| Se sigue trabajando en la produccion del video demostrativo. (utilizacion de Blender)|
|24/05/2019| DC| Se Actualizo la bitacora!|
















