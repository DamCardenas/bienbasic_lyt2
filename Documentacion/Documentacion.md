# Tecnologico Nacional de Mexico

## BB (Bien  Basic)
## Autores:
- Damian Cardenas Quiñones
- Karen Torrijos Galindo
- David Villanueva


## Descripcion
Compilador de lenguaje tipo "Basic" (con unas cuantas variantes ludicas) escrito en javascript (node.js) para la clase de "Compiladores y Automatas II".

## Historia

Este proyecto fue iniciado usando el lenguaje JAVA, pero debido a circunstancias relacionadas con el tiempo disponible para realizarlo eran menores, se opto por migrar el proyecto al lenguaje interpretado de javascript; otro motivo es la facilidad de creacion de frontend amigable con tecnologias web usando "Electron.js" para generar el GUI del editor "IDE" .

Este compilador tiene una gran particularidad dado que al ser escrito con el lenguaje de javascript su proceso de produccion fue muy acelerado y eficaz, dado a que permitio a los participantes del equipo escribir la aplicacion de manera mas rapida debido a la curva de aprendizaje sencilla y la facilidad de realizar algunas tareas a comparacion de los demas lenguajes de programacion propuestos en un principio (java, c#, c, c++).

## 



## Palabras Reservadas:

|Palabra|equivalentes|Funcion|
|:-:|:-:|:-:|
|_base\_| main | bloque de codigo principal|
|flotante o %f|float|denotar tipo flotante|
|entero o %e |int|denotar tipo etero|
|cadena o %cn|string|denotar tipo cadena|
|caracter o %c|char|denotar tipo caracter|
|iniciar|~|denotar el inicio de un bloque de codigo|
|terminar|~| denotar el final de un bloque de codigo|
|si|if| ~
|sino|else|~
|mientras|while|~
|para|for|~
|cuando|switch|~
|sea|case|~
|romper| break| ~|

## Tokens

|Token|Expresion Regular|
|:-:|:-:|
|V_ENTERO| [0-9]+ |
|V_FLOTANTE| [0-9]*\\.[0-9]\+ |
|V_CADENA| ".*"|
|V_CARACTER| '.'|
|T_ENTERO| entero |
|T_FLOTANTE| flotante |
|T_CADENA| cadena|
|T_CARACTER| caracter|
|INICIAR|iniciar |
|TERMINAR| terminar |
|SI| si |
|SINO| sino |
|MIENTRAS| mientras |
|PARA| para |
|CUANDO| cuando|
|SEA| sea |
| ROMPER | romper|
|\_BASE\_|\_base\_|
|COMENTARIO_L| #.* |
|COMENTARIO_ML| ##.*## |
|IDENTIFICADOR| [A-Za-z_@]+[A-Za-z0-9_@]*|
|INCREMENTO|++|
|SUMA|+|
|DECREMENTO|--|
|RESTA|-|
|RAIZ|//|
|DIVISION|/|
|POTENCIA|\*\*|
|MULTIPLICACION|*|
|PARENTESIS_A|\(|
|PARENTESIS_C|\)|
|CORCHETE_A|\[|
|CORCHETE_C|\]|
|LLAVE_A|\{|
|LLAVE_C|\}|

## Analisis Lexico

Se opto por escribir nuestro propio lexer para que su implementacion fuese a la medida del proyecto.
Para realizar el analisis lexico de Bien Basic se requieren 3 clases y un archivo json.



```json
[
    {
        "token":"V_FLOTANTE",
        "regex":"[0-9]*\\.[0-9]*"
    },
    {
        "token":"V_ENTERO",
        "regex":"[0-9]+"
    },
    {
        "token":"ERROR_6",
        "regex":"\\\""
    },
    {
        "token":"ERROR_7",
        "regex":"'"
    },
    {
        "token":"ERROR_3",
        "regex":"\\\"[^\\\"]*"
    },
    {
        "token":"V_CADENA",
        "regex":"\\\".*\\\""
    },
    {
        "token":"ERROR_4",
        "regex":"'."
    },
    {
        "token":"V_CARACTER",
        "regex":"\\'.\\'"
    },
    {
        "token":"T_ENTERO",
        "regex":"(entero|\\%e)"
    },
    {
        "token":"T_FLOTANTE",
        "regex":"(flotante|\\%f)"
    },
    {
        "token":"T_CADENA",
        "regex":"(cadena|\\%ca)"
    },
    {
        "token":"T_CARACTER",
        "regex":"(caracter|\\%c)"
    },
    {
        "token":"INICIAR",
        "regex":"iniciar"
    },
    {
        "token":"VERDADERO",
        "regex":"verdadero"
    },
    {
        "token":"FALSO",
        "regex":"falso"
    },

    {
        "token":"TERMINAR",
        "regex":"terminar"
    },
    {
        "token":"SI",
        "regex":"si"
    },
    {
        "token":"SINO",
        "regex":"sino"
    },
    {
        "token":"MIENTRAS",
        "regex":"mientras"
    },
    {
        "token":"PARA",
        "regex":"para"
    },
    {
        "token":"CUANDO",
        "regex":"cuando"
    },
    {
        "token":"SEA",
        "regex":"sea"
    },
    {
        "token":"ROMPER",
        "regex":"romper"
    },
    {
        "token":"_BASE_",
        "regex":"_base_"
    },
    {
        "token":"COMENTARIO_L",
        "regex":"#[^#$\\n]*"
    },
    {
        "token":"COMENTARIO_ML",
        "regex":"#{2}[^#]*#{2}"
    },
    {
        "token":"IDENTIFICADOR",
        "regex":"[A-Za-z_][A-Za-z0-9_]*"
    },
    {
        "token":"==",
        "regex":"=="
    },
    {
        "token":"!=",
        "regex":"!="
    },
    {
        "token":">=",
        "regex":">="
    },
    {
        "token":"<=",
        "regex":"<="
    },
    {
        "token":"<",
        "regex":"<"
    },
    {
        "token":">",
        "regex":">"
    },
    {
        "token":"&&",
        "regex":"&{2}"
    },
    {
        "token":"||",
        "regex":"\\|\\|"
    },
    {
        "token":"!",
        "regex":"!"
    },
    {
        "token":"=",
        "regex":"="
    },
    {
        "token":"++",
        "regex":"\\+\\+"
    },
    {
        "token":"+",
        "regex":"\\+"
    },
    {
        "token":"--",
        "regex":"--"
    },
    {
        "token":"-",
        "regex":"-"
    },
    {
        "token":"//",
        "regex":"//"
    },
    {
        "token":"/",
        "regex":"/"
    },
    {
        "token":"**",
        "regex":"\\*\\*"
    },
    {
        "token":"*",
        "regex":"\\*"
    },
    {
        "token":"(",
        "regex":"\\("
    },
    {
        "token":")",
        "regex":"\\)"
    },
    {
        "token":"[",
        "regex":"\\["
    },
    {
        "token":"]",
        "regex":"\\]"
    },
    {
        "token":"{",
        "regex":"\\{"
    },
    {
        "token":"}",
        "regex":"\\}"
    },
    {
        "token":"FIN_L",
        "regex":"\\n"
    },
    {
        "token":"ESCAPE",
        "regex":"[\\r\\t\\s]"
    },
    {
        "token":",",
        "regex":","
    },
    {
        "token":"ERROR_1",
        "regex":"&"
    },
    {
        "token":"ERROR_8",
        "regex":"\\|"
    } ,
    {
        "token":"ERROR_2",
        "regex":"%"
    }   
]

```

## Analisis Sintactico
## Gramaticas








