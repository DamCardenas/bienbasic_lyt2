# Manual Tecnico

|Palabra Reservada|Descripcion|
|:-|:-|
|iniciar|Sirve para marcar la pauta de inicio de un programa o una funcion.|
|terminar|Sirve para marcar el termino de un programa, una funcion, un condicional, etc...|
|_ base _|Nombre de la Funcion principal (Main) del programa principal. |
|entero, %e|Se refiere al tipo de dato entero, se puede usar cualquiera de las dos formas|
|flotante, %f|Se refiere al tipo de dato flotante, se puede usar cualquiera de las dos formas|
|caracter, %c|Se refiere al tipo de dato caracter, se puede usar cualquiera de las dos formas|
|cadena, %ca|Se refiere al tipo de dato cadena, se puede usar cualquiera de las dos formas|
|verdadero|Se refiere a un valor booleano positivo, es equivalente al valor entero 1|
|falso|Se refiere a un valor booleano negativo, es equivalente al valor entero 0|
|si|Se refiere al condicional normalmente conocido como "if"|
|imprimir|Se refiere a la funcion util para desplegar un mensaje en pantalla|

|Operadores de Asignacion|
|-|
|=|

|Operadores de Comparacion|
|-|
|<|
|>|
|<=|
|>=|
|==|
|!=|

|Operadores Logicos|
|-|
|!|
|&&|
|\|\||

|Operadores de Modificacion|Descripcion|
|-|-|
|++|Operador de incremento|
|--|Operador de decremento|

|Operadores Aritmeticos|Descripcion|
|-|-|
|+| Operador aritmetico de Adicion|
|-| Operador aritmetico de Sustraccion|
|*| Operador aritmetico de Multiplicacion|
|/| Operador aritmetico de Division| 
|//| Operador aritmetico de Raiz|
|**| Operador aritmetico de Potencia|

|Simbolos de Agrupacion|
|-|
|(|
|)|

|Simbolos de Separacion|
|-|
|,|