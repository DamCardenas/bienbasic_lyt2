# Manual de usuario
![alt text](../Bien_Basic_logo.png "Logo Title Text 1")


## Repositorio

https://bitbucket.org/DamCardenas/bienbasic_lyt2

## Descripcion

Este manual sirve para orientar al usuario en ambitos de ejecucion y utilizacion del lenguaje y las herramientas del lenguaje de programacion Bien Basic.

Bien Basic es un lenguaje de programacion de sintactica y lexica sencilla.
Su compilador esta escrito en javascript (nodejs).

## Composicion

El proyecto incluye un IDE y el compilador como tal.
El IDE sirve para editar el codigo con extension ".bb" y hacer uso del compilador de manera visual.
El Compilador tiene la facultad de usarse en forma de terminal o como se menciona anteriormente, con apoyo del IDE.


## Instalacion 

Para instalar el proyecto primero se deben cumplir con los sig. Pasos

### Pasos de Instalacion

1. Instalar nodejs en su version 10.15.3 o superior.
2. Descargar repositorio actualizado.
3. Instalar modulos del proyecto.

### Instalacion de nodejs

#### Windows

1. entrar en el sitio https://nodejs.org/es/download/ y seleccionar la opcion segun la arquitectura del  procesador del equipo en el que se desea instalar.
2. ejecutar el archivo descargado.

#### Linux

ingresar el siguiente comando en la terminal

##### Ubuntu
```bash
sudo apt install nodejs
```
##### Arch-Linux
```bash
pacman -y install nodejs
```
### Descargar el repositorio

Para descargar el repositorio se debe ingresar a la sig. Liga https://bitbucket.org/DamCardenas/bienbasic_lyt2/get/7c3278305d93.zip.<br>
En automatico, el repositorio debe empezar a descargarse.

### Instalar modulos del proyecto

Este proceso es el mismo para Windows, Linux y la plataforma que se desee.

1. abrir una terminal.
2. dirigirse al directorio  donde esta guardado el proyecto.
3. ejecutar el siguiente comando.

```bash
npm start
```


## Ejecucion



## Interfaz 

## Sintaxis

## 
