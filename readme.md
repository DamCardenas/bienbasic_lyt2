# Bien Basic

## Autores
- Damian Cardenas Quiñones
- David Villanueva
- Karen Torrijos Galindo

## Ejecucion

Para ejecutar la aplicacion se necesita instalar nodejs.
una vez instalado, hay que abrir un shell ingresar al directorio raiz del proyecto (desde el shell obviamente)
y ejecutar los siguientes comandos:

```bash
npm install 
npm start
```