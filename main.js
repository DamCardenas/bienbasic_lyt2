const electron = require("electron");
const {app,BrowserWindow} = electron;
const main_width=800;
const main_height=600;
var openWindow=(filename,_width,_height,_minWidth=_width,_minHeight=_height,_transparent=false,_frame=true,_aot=false,_debug=false,_max=false)=>{
    let win=new BrowserWindow({icon:"./Bien_Basic_logo.png",width:_width,height:_height,minWidth:_minWidth,minHeight:_minHeight,transparent:_transparent,frame:_frame,alwaysOnTop:_aot});
    if(_max)win.maximize();
    if(_debug)win.webContents.openDevTools();
    win.loadURL(`file://${__dirname}/`+filename+`.html`);
}
app.on("ready",()=>{
    //openWindow("Splash",310,445,300,440,true,false);
    openWindow("main",990,700,990,700,true,true);
});
app.on("window-all-closed",()=>{
    app.quit()
});

module.exports={openWindow};
