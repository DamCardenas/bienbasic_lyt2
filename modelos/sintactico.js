const pegjs = require('pegjs');
const fs=require('fs');
class aSintactico
{
    static analizar(input="",grammar="./modelos/gramaticas.pegjs")
    {
        var parser=  pegjs.generate(fs.readFileSync(grammar,'utf-8'));
        return parser.parse(input);
    } 
}
module.exports = aSintactico;