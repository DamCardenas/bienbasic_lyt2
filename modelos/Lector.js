'use strict';
class Lector
{
    constructor(input="")
    {
        this.code=input;
        this.index=-1;
        this.start=null;
        this.line=1;
        this.column=1;
    }
    // visualiza el caracter actual
    actualChar()
    {
        if(this.index==-1)
            return "♪";
        return this.code[this.index];
    }
    // Previzualiza el siguiente caracter pero no modifica el buffer
    nextChar()
    {
        if(this.index<this.code.length)
            return this.code[this.index+1];
        else
            return '♫';// Fin del codigo
    }
    hasNext()
    {
        return this.nextChar()!='♫';
    }
    // Previsualiza el caracter anterior paro no modifica el buffer
    prevChar()
    {
        if(!(this.index>0))return "♪"; //inicio del codigo
        return this.code[this.index-1];
    }
    next()
    {
        if(this.index<this.code.length)
        {
            this.column++;
            return this.code[++this.index];
        }
        else
            return '♫';// Fin del codigo
    }
    newLine()
    {
        this.line++;
        this.column=1;
    }
    back()
    {
        if(!(this.index>0))return "♪"; //inicio del codigo
        if(this.column==1)
            this.line--;
        else
            this.column--;
        return this.code[--this.index];
    }
    setStart()
    {
        this.start=this.index;
        return this.actualChar();
    }
    unsetStart()
    {
        this.start=null;
    }
    buffer()
    {
        if(this.start<0 || this.start==null)return "";
        var out="";
        for(var i=this.start;i<=this.index;i++)
            out+=this.code[i];
        return out;
    }
}
module.exports=Lector;