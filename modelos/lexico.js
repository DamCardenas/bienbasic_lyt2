'use strict';
//const Flexjs=require('flex-js');
const Lector=require('./Lector');
const Token=require('./token');
const reglas=require('./regex.json');
class aLexico
{
    static analizar(input="")
    {
        if(!input||input.length==0)return null;
        var output = {tokens:[], errores:[]};
        var lec=new Lector(input);
        var estado=0;
        while(lec.hasNext())
        {
            switch(estado)
            {
                case 0:
                    lec.next();
                    var tok= aLexico.check(lec.actualChar())
                    if(tok!=null&&tok!="ESCAPE")// Descarte de tokens no utiles  y caracteres de escape
                    {
                        lec.setStart();
                        estado=1;
                    }
                    if(tok=="FIN_L")
                        lec.newLine();
                break;
                case 1:
                    while(aLexico.check(lec.buffer())!=null&&lec.hasNext())
                        lec.next();
                    lec.back();
                    var tok = aLexico.check(lec.buffer());
                    if(tok.match("ERROR_.*"))
                        output.errores.push(new Token(tok,lec.buffer(),lec.line,lec.column));
                    else 
                        if(tok!="COMENTARIO_L"&&tok!="COMENTARIO_ML")
                            output.tokens.push(new Token(tok,lec.buffer(),lec.line,lec.column));
                    estado = 0;
                    lec.unsetStart();
                break;
            }
        }
        return output;
    }
    static test(input="")
    {
        if(!input||input.length==0)return null;
        var output = {tokens:[], errores:[]};
        var lec=new Lector(input);
        var estado=0;
        while(lec.hasNext())
            switch(estado)
            {
                case 0:
                    console.log("........"+estado+".........");
                    lec.next();
                    var tok= aLexico.regextest(lec.actualChar())
                    if(tok!=null&&tok!="ESCAPE"&&tok!="FIN_L")// Descarte de tokens no utiles  y caracteres de escape
                    {
                        lec.setStart();
                        if(lec.hasNext())
                        {
                            estado=1;
                            lec.next();
                        }
                    }
                    if(tok=="FIN_L")
                        lec.newLine();
                    
                    console.log("caracter Actual: "+lec.actualChar());
                    console.log("token Encontrado: "+tok);
                    console.log("buffer Actual: "+lec.buffer());
                    console.log("Estado Siguiente: "+estado);

                break;
                case 1:
                    console.log("........"+estado+".........");
                    
                    while(aLexico.regextest(lec.buffer())!=null&&lec.hasNext())
                        lec.next();
                    lec.back();
                    tok =aLexico.regextest(lec.buffer())
                    if(tok.match("ERROR_.*"))
                        output.errores.push(new Token(tok,lec.buffer(),lec.line,lec.column));
                    else 
                        if(tok!="COMENTARIO_L"&&tok!="COMENTARIO_ML")
                            output.tokens.push(new Token(tok,lec.buffer(),lec.line,lec.column));
                    estado = 0;
                    console.log("caracter Actual: "+lec.actualChar());
                    console.log("token Encontrado: "+tok);
                    console.log("buffer Actual: "+lec.buffer());
                    console.log("Estado Siguiente: "+estado);
                    lec.unsetStart();
                break;
            }
        return output;
    }
    static regextest(input="")
    {
        if(!input)
        {
            console.log("Entrada no establecida");
            return null;
        }
        console.log(".....REGEXTEST.....");
        for(var i=0; i<reglas.length;i++)
        {
            console.log(`...... ${i} .......`);
            console.log("cadena: "+input);
            console.log("regex: "+reglas[i].regex);
            console.log("token: "+reglas[i].token);
            if(input.match("^"+reglas[i].regex+"$"))
            {
                console.log("Regla aceptada!");
                console.log("..... FIN .....");
                return reglas[i].token;
            }
        }
        console.log("indefinido")
        return null;
    }
    static check(input="")
    {
        if(!input)return null;
        for(var i=0; i<reglas.length;i++)
            if(input.match("^"+reglas[i].regex+"$"))
                return reglas[i].token;
        return null;
    }
    static BB_sint_compound(toks=[])
    {
        var output="";
        toks.forEach(element => {
            output+=element.token+" ";
            if(element.token=="FIN_L")output+="\n"
        });
        return output;
    }
}
module.exports=aLexico;