{
function deleteSpaces(arr)
{
	for( var i = 0; i < arr.length; i++)
    {
    	if ( arr[i] === " ") 
         arr.splice(i, 1);
        else
        {
        	
        	if(Array.isArray(arr[i]))
       			arr[i]=deleteSpaces(arr[i])
        }
       
    }
    return arr;
}


class Nodo
{
	constructor(_titulo="",_hijos=null)
    {
    	this.titulo=titulo;
        this.hijos=_hijos;
    }
    addHijo(titulo="")
    {
        this.hijos.push(new Nodo(titulo));
    }
    getHijo(index=-1)
    {
        if(index<0)return null;
        return this.hijos[index];
    }
}

class Arbol
{
	constructor()
    {
    	this.raiz=new Nodo("programa");
    }
    addNodo()
}


}
// Sintaxis principal del programa
Program= inicio:(_ iniciar " " base _)  contenido:((Sentencia/Condicionales)*)  final:(_ terminar _ base) {
return {ini:deleteSpaces(inicio),cont:deleteSpaces(contenido),fin:deleteSpaces(final)} }

// Sentencia Basica
Sentencia= dec:(DecSimple/asig){return dec }




// Declaracion de variables
DecSimple = dec:(_ tipo _ Dec1 _ Dec2 _) {return [deleteSpaces(dec)]} 
     
Dec1= ((_ asig _) / (_ id _))
Dec2= dex2:(","_ Dec1 _)* {return [deleteSpaces(dex2)]}



// Expresiones matematicas
Expr= matExpr:((_ Expr1 _ oper _ Expr _)/ psis_a _ Expr _ psis_c /Expr1 ){
return [deleteSpaces(matExpr)]
}
Expr1= (id/valor)

//Condicionales

Condicionales= SI/MIENTRAS

// SI
SI= "si" _ psis_a _ boolExpr _ psis_c _ (Sentencia/Condicionales)* _  terminar _ "si"


// MIENTRAS 
MIENTRAS = "mientras" _ psis_a _ boolExpr _ psis_c _ (Sentencia/Condicionales)* _  terminar _ "mientras" 



// Expresiones booleanas
boolExpr= boolExpr:(_ boolExpr1 _ booloper_comp _ boolExpr _/_ boolExpr1 _ booloper_log _ boolExpr _   / boolExpr1){
return [deleteSpaces(boolExpr)];
}
boolExpr1= (Expr1/Expr/boolExpr2/boolExprp)
boolExprp=_ psis_a _  boolExpr _  psis_c _
boolExpr2 = "verdadero"/"falso" 

base="_base_"
iniciar="iniciar"
terminar="terminar"
psis_a="("
psis_c=")"
oper="+"/"-"/"*"/"/"/"**"/"//"
booloper_comp="=="/"!="/"<="/">="/"<"/">"
booloper_log="&&"/"||"
id= [A-Za-z_][A-Za-z0-9_]* {return text()}
asig= asig:(_ id _ "="_ (valor/Expr)){ return [deleteSpaces(asig)]} 
valor= flotante/entero/cadena/caracter
entero=[0-9]+ {return text()}
flotante= [0-9]*"."[0-9]+ {return text()}
cadena="\"".*"\"" {return text()}
caracter= "'".*"'" {return text()}

tipo= (("entero"/"%e")/("flotante"/"%f")/("cadena"/"%ca")/("caracter"/"%c") ) {return text()}
 
_ "whitespace"= [ \t\n\r]*{ return " ";}
