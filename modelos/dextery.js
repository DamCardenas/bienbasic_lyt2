const fs =require('fs');
class Dextery
{
    static convert(input="")
    {
        var code="iniciar\ncargar 1 null r_x\ncargar 12.4 null r_y\nadicion r_x r_y r_t1\nimprimir null null r_t1\nterminar\n";
        fs.writeFileSync('output.bbci',code);
    }
}
module.exports=Dextery;
/*
-- CN --
%e x = 1
%f y = 12.4
imprimir(x+y)
-- CI -- 
iniciar
cargar 1 null r_x
cargar 12.4 null r_y
adicion r_x r_y r_t1
imprimir null null r_t1
terminar
*/