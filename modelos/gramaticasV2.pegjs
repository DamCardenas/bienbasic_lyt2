{
function deleteSpaces(arr)
{
	for( var i = 0; i < arr.length; i++)
    {
    	if ( arr[i] === " ") 
         arr.splice(i, 1);
        else
        {
        	
        	if(Array.isArray(arr[i]))
       			arr[i]=deleteSpaces(arr[i])
        }
       
    }
    return arr;
}
}
// Sintaxis principal del programa
Program= inicio:(_ iniciar " " base _)  contenido:((Sentencia/Condicionales)*)  final:(_ terminar _ base) {
return {ini:deleteSpaces(inicio),cont:deleteSpaces(contenido),fin:deleteSpaces(final)} }

// Sentencia Basica
Sentencia= sent:(Imprimir/DecSimple/asig){return sent}

// imprimir
Imprimir="imprimir" _ "(" _ (valor/id) _ ")"


// Declaracion de variables
DecSimple = _ tipo:(tipo) _ dec:(Dec1) _ dec2:(Dec2) _ 
     
Dec1= _ asignacion:(asig) _/ _ id:(id) _ 
Dec2= (","_ Dec1 _)* 



// Expresiones matematicas
Expr= _ a:Expr1 _ oper:oper _ b:Expr _ 
/ psis_a _ Expr _ psis_c _ oper _ Expr /Expr1
Expr1= (id/valor)
//Condicionales

Condicionales= SI/MIENTRAS

// SI
SI= "si" _ psis_a _ boolExpr _ psis_c _ (Sentencia/Condicionales)* _  terminar _ "si"


// MIENTRAS 
MIENTRAS = "mientras" _ psis_a _ boolExpr _ psis_c _ (Sentencia/Condicionales)* _  terminar _ "mientras" 



// Expresiones booleanas
boolExpr= boolExpr:(_ boolExpr1 _ booloper_comp _ boolExpr _/_ boolExpr1 _ booloper_log _ boolExpr _   / boolExpr1)
boolExpr1= (Expr1/Expr/boolExpr2/boolExprp)
boolExprp=_ psis_a _  boolExpr _  psis_c _
boolExpr2 = "verdadero"/"falso" 

base="_base_"
iniciar="iniciar"
terminar="terminar"
psis_a="("
psis_c=")"
oper="+"/"-"/"*"/"/"/"**"/"//"
booloper_comp="=="/"!="/"<="/">="/"<"/">"
booloper_log="&&"/"||"
id= [A-Za-z_][A-Za-z0-9_]* {return {token:"IDENTIFICADOR",lexema:text()}}
asig= _ identificador:id _ "="_ expresion:(valor/Expr) {return {"=":{identificador,expresion}}}
valor= (vflotante/ventero/vcadena/vcaracter)
ventero=[0-9]+ {return {token:"V_ENTERO",lexema:text()}}
vflotante= [0-9]*"."[0-9]+ {return {token:"V_FLOTANTE",lexema:text()}}
vcadena="\""(.*)"\"" {return {token:"V_CADENA",lexema:text()}}
vcaracter= "'".*"'" {return {token:"V_CARACTER",lexema:text()}}
tipo= tentero/tflotante/tcadena/tcaracter {return }
tentero="entero"/"%e" {return {token:"T_ENTERO",lexema:text()}}
tflotante="flotante"/"%f"{return {token:"T_FLOTANTE",lexema:text()}}
tcadena="cadena"/"%ca"{return {token:"T_CADENA",lexema:text()}}
tcaracter="caracter"/"%c"{return {token:"T_CARACTER",lexema:text()}}

_ "whitespace"= [ \t\n\r]*{ return " ";}
