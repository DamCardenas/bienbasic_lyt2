
// Sintaxis principal del programa
Program= _ iniciar " " base _ fin _ (Sentencia/Condicionales/fin)* _ terminar _ base  _ fin*

// Sentencia Basica
Sentencia= (_(Imprimir/DecSimple/asig)_ fin _)


Imprimir="IMPRIMIR" _ "(" _ (Expr) _ ")"

// Declaracion de variables
DecSimple = head:(_ tipo _) tail:(_ Dec1 _ Dec2 _)
Dec1= ((_ asig _) / (_ id _))
Dec2= (","_ Dec1 _)* 



// Expresiones matematicas
Expr= (_ Expr1 _ oper _ Expr _)/ psis_a _ Expr _ psis_c /Expr1 
Expr1= (id/valor)

//Condicionales

Condicionales= SI/MIENTRAS

// SI
SI= "SI" _ psis_a _ boolExpr _ psis_c _ fin _ (Sentencia/Condicionales/fin )* _  terminar _ "SI" _ fin*


// MIENTRAS 
MIENTRAS = "MIENTRAS" _ psis_a _ boolExpr _ psis_c _ fin _ (Sentencia/Condicionales/fin)* _  terminar _ "MIENTRAS" _ fin*



// Expresiones booleanas
boolExpr= _ boolExpr1 _ booloper_comp _ boolExpr _/_ boolExpr1 _ booloper_log _ boolExpr _   / boolExpr1
boolExpr1= (Expr1/Expr/boolExpr2/boolExprp)
boolExprp=_ psis_a _  boolExpr _  psis_c _
boolExpr2 = "VERDADERO"/"FALSO" 

base="_BASE_"
iniciar="INICIAR"
terminar="TERMINAR"
psis_a="("
psis_c=")"
oper="+"/"-"/"*"/"/"/"**"/"//"
booloper_comp="=="/"!="/"<="/">="/"<"/">"
booloper_log="&&"/"||"
id= "IDENTIFICADOR"
asig= _ id _ "="_ (valor/Expr) 
valor= "V_ENTERO"/"V_FLOTANTE"/"V_CADENA"/"V_CARACTER"
tipo= "T_ENTERO"/"T_FLOTANTE"/"T_CADENA"/"T_CARACTER"
fin = _ "FIN_L" _ 
_ "whitespace"= [ \t\n\r]*
